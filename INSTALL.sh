#!/usr/bin/env bash

set -e # Exit as soon as any line in the bash script fails

ROOTDIR=$( cd $(dirname $0) ; pwd -P ) # path to main TargetNGS directory

echo
exec &> >(tee -a  install.log)
exec 2>&1 # copies stderr onto stdout

# create a directory where all dependencies will be installed
cd $ROOTDIR

export PATH="$ROOTDIR/ext/miniconda/bin:$ROOTDIR/ext:$PATH"

minicondaPATH=$ROOTDIR/ext/miniconda
mkdir -p $minicondaPATH

# minimum tools version
minimap2_VER=2.26
miniasm_VER=0.3
flye_VER=2.9.2
racon_VER=1.5.0
samtools_VER=1.17
wtdbg_VER=2.5

install_wtdbg(){
echo "--------------------------------------------------------------------------
                           Installing wtdbg v$wtdbg_VER
--------------------------------------------------------------------------------
"
wget https://github.com/ruanjue/wtdbg2/releases/download/v2.5/wtdbg-2.5_x64_linux.tgz
tar xvzf wtdbg-2.5_x64_linux.tgz 
mkdir -p $minicondaPATH/bin
cp wtdbg-2.5_x64_linux/* $minicondaPATH/bin/
rm -f  wtdbg-2.5_x64_linux.tgz
rm -rf wtdbg-2.5_x64_linux
#conda install --yes -p $minicondaPATH -c bioconda wtdbg
echo "
--------------------------------------------------------------------------------
                           wtdbg v$wtdbg_VER installed
--------------------------------------------------------------------------------
"

}

install_minimap2()
{
echo "--------------------------------------------------------------------------
                           Installing minimap2 v$minimap2_VER
--------------------------------------------------------------------------------
"
mamba install --yes -p $minicondaPATH -c bioconda minimap2=$minimap2_VER
echo "
--------------------------------------------------------------------------------
                           minimap2 v$minimap2_VER installed
--------------------------------------------------------------------------------
"

}

install_flye()
{
echo "--------------------------------------------------------------------------
                           Installing flye v$flye_VER
--------------------------------------------------------------------------------
"
wget -O ext/Flye-$flye_VER.tar.gz  https://github.com/fenderglass/Flye/archive/$flye_VER.tar.gz
cd ext && tar xzf Flye-$flye_VER.tar.gz && rm Flye-$flye_VER.tar.gz
cd Flye-$flye_VER
make >/dev/null || make
ln -s $PWD/bin/flye $minicondaPATH/bin/
#python3 setup.py install 
echo "
--------------------------------------------------------------------------------
                           flye v$flye_VER installed
--------------------------------------------------------------------------------
"
}

install_miniasm()
{
echo "--------------------------------------------------------------------------
                           Installing miniasm v$miniasm_VER
--------------------------------------------------------------------------------
"
mamba install --yes -p $minicondaPATH -c bioconda miniasm
echo "
--------------------------------------------------------------------------------
                           miniasm v$miniasm_VER installed
--------------------------------------------------------------------------------
"

}

install_racon()
{
echo "--------------------------------------------------------------------------
                           Installing racon v$racon_VER
--------------------------------------------------------------------------------
"
mamba install --yes -p $minicondaPATH -c bioconda racon=$racon_VER
echo "
--------------------------------------------------------------------------------
                           racon v$racon_VER installed
--------------------------------------------------------------------------------
"

}
install_samtools()
{
echo "--------------------------------------------------------------------------
                           Downloading samtools v$samtools_VER
--------------------------------------------------------------------------------
"
mamba install --yes -p $minicondaPATH -c bioconda samtools=$samtools_VER
echo "
--------------------------------------------------------------------------------
                           samtools v$samtools_VER installed
--------------------------------------------------------------------------------
"
}

install_miniconda()
{
echo "--------------------------------------------------------------------------
                           downloading miniconda
--------------------------------------------------------------------------------
"
if [[ "$OSTYPE" == "darwin"* ]]
then
{

  curl -k -A "Mozilla/5.0" -L -o ext/miniconda.sh https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh

}
else
{  

  curl -k -A "Mozilla/5.0" -L -o ext/miniconda.sh https://repo.anaconda.com/miniconda/Miniconda3-py39_23.1.0-1-Linux-x86_64.sh
}
fi

chmod +x ext/miniconda.sh
./ext/miniconda.sh -b -p $minicondaPATH -f
export PATH=$ROOTDIR/ext/miniconda/bin:$PATH
conda install -n base conda-forge::mamba


}

checkSystemInstallation()
{
    IFS=:
    for d in $PATH; do
      if test -x "$d/$1"; then return 0; fi
    done
    return 1
}

checkLocalInstallation()
{
    IFS=:
    for d in $minicondaPATH/bin; do
      if test -x "$d/$1"; then return 0; fi
    done
    return 1
}

versionStr() {
  echo "$@" | awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }';
}


####### MAIN #######


if ( checkSystemInstallation conda )
then
	conda_installed_VER=`conda --version 2>&1|perl -nle 'print $& if m{(\d+\.\d+\.\d+)}'`;
  	echo " - found conda $conda_installed_VER"
	echo "conda create --prefix $minicondaPATH -y"
        conda create --prefix $minicondaPATH -y
else
 	install_miniconda
fi

if ( checkSystemInstallation wtdbg2 )
then
	wtdbg_installed_VER=`wtdbg2 -V |  perl -nle 'print $1 if m{(\d+\.\d+\.*\d*)}'`;
	if [ $(versionStr $wtdbg_installed_VER) -ge $(versionStr $wtdbg_VER) ]
	then
		echo " - found wtdbg $wtdbg_installed_VER"
	else
		install_wtdbg
	fi
else
	install_wtdbg
fi

if ( checkSystemInstallation minimap2 )
then
	minimap2_installed_VER=`minimap2 --version |  perl -nle 'print $1 if m{(\d+\.\d+\.*\d*)}'`;
	if [ $(versionStr $minimap2_installed_VER) -ge $(versionStr $minimap2_VER) ]
	then
		echo " - found minimap2 $minimap2_installed_VER"
	else
		install_minimap2
	fi
else
	install_minimap2
fi

if ( checkSystemInstallation miniasm )
then
	miniasm_installed_VER=`miniasm -V |  perl -nle 'print $1 if m{(\d+\.\d+\.*\d*)}'`;
	if [ $(versionStr $miniasm_installed_VER) -ge $(versionStr $miniasm_VER) ]
	then
		echo " - found miniasm $miniasm_installed_VER"
	else
		install_miniasm
	fi
else
	install_miniasm
fi

if ( checkSystemInstallation flye )
then
	flye_installed_VER=`flye -v 2>&1 |  perl -nle 'print $1 if m{(\d+\.\d+\.*\d*)}'`;
	if [ $(versionStr $flye_installed_VER) -ge $(versionStr $flye_VER) ]
	then
		echo " - found flye $flye_installed_VER"
	else
		install_flye
	fi
else
	install_flye
fi

if ( checkSystemInstallation racon )
then
	racon_installed_VER=`racon --version | perl -nle 'print $1 if m{v(\d+\.\d+\.*\d*)}'`;
	if [ $(versionStr $racon_installed_VER) -ge $(versionStr $racon_VER) ]
        then
		echo " - found racon $racon_installed_VER"
	else
		install_racon
	fi
else
	install_racon
fi

#if ( checkSystemInstallation samtools )
#then
#	samtools_installed_VER=`samtools 2>&1| grep 'Version'|perl -nle 'print $& if m{Version: \d+\.\d+.\d+}'`;
#	if [ -z "$samtools_installed_VER" ]
#	then 
#		samtools_installed_VER=`samtools 2>&1| grep 'Version'|perl -nle 'print $& if m{Version: \d+\.\d+}'`; 
#	fi
#	
#	if  ( echo $samtools_installed_VER $samtools_VER| awk '{if($2>=$3) exit 0; else exit 1}' )
#	then
#		echo " - found samtools $samtools_installed_VER"
#	else
#		install_samtools
#	fi
#else
#  install_samtools
#fi

conda clean -y -a


echo "
==============================================
	Installation  successfully
==============================================

	./lrasm  -h

for usage.
Read the README for more information!
Thanks!
";
