#!/usr/bin/env bash
set -e
rootdir=$( cd $(dirname $0) ; pwd -P )
tempdir=$rootdir/temp


test_result(){
	Test=$rootdir/lrasm_out/contigs.fa
	#Expect=$rootdir/contigs.fa
	testName="run test";
	if [[ $(find $Test -type f -size +44000c 2>/dev/null) ]]
        then
                grep -c ">" $Test | awk '{print "Contigs number: " $1}'
                echo "$testName finished"
		touch "test.success"
        else
		echo "$testName failed!"
		touch "test.fail"
        fi
}

cd $rootdir
echo "Working Dir: $rootdir";
echo "Running Test ..."

rm -f "test.*"

$rootdir/../lrasm  -e -t 4 reads.fastq.gz || true

test_result;
