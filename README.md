# Long reads assembly

## Installation
Download the latest version of long_read_assembly from [gitlab](https://gitlab.com/chienchi/long_read_assembly) or use `git clone` from command line.

```
git clone https://gitlab.com/chienchi/long_read_assembly.git
```

`cd` into the `long_read_assembly` directory

```
cd long_read_assembly
./INSTALL.sh 
```
The `sudo` privileges are not needed for installation. The installation script will pull required dependencies with internet. A log of all installation can be found in `install.log`

##Dependencies
long_read_assembly run requires following dependencies which should be in your path. All of the dependencies will be installed by `INSTALL.sh`.

### Programming/Scripting languages
- [Perl >=v5.16.3](https://www.perl.org/get.html)
    - The pipeline has only been tested in v5.16.3

### Third party softwares/packages
- [minimap2 (2.24)](https://github.com/lh3/minimap2) - Reads Aligner
- [miniasm (0.3)](https://github.com/lh3/miniasm) - OLC-based de novo assembler for noisy long reads 
- [wtdbg2 (2.5)](https://github.com/ruanjue/wtdbg2) - fuzzy Bruijn graph (FBG) assembler for long noisy reads assembly
- [flye (2.9.1)](https://github.com/fenderglass/Flye) - De novo assembler for single molecule sequencing reads using repeat graphs
- [racon (1.5.0)](https://github.com/isovic/racon) - Consensus module for raw de novo DNA assembly of long uncorrected reads

 
## Running

```
Usage: lrasm reads.fasta/q ...
Version:
Output:
    -o     STR     Output directory [default: miniasm]
    -p     STR     Output file prefix [default: contigs]
Options:
    -a     STR     Assembler, miniasm or wtdbg2 or flye [default:miniasm]
    -e             Error correct reads using racon before assembly 
    --eco  STR     Error correction options of racon [--fragment-correction --quality-threshold 7]
    --olo  STR     Overlap options for minimap2 self-alignment [-x ava-ont]
    --mo   STR     Mappping to contigs/reference options for minimap2 of consensus run [-x map-ont]
    --co   STR     Consensus generation options for racon []
    --ao   STR     Miniasm options []
    --wo   STR     wtdbg2 options []
    --fo   STR     flye options [], ex: --meta for metagenome
    -t     INT     Number of threads [1]
    -n     INT     Number of iteractions for consensus [3]
    -h             Show this help message. 
    -v             Print version.
Preset:
    -x     STR     preset 
                   ont: --olo "-x ava-ont" --mo "-x map-ont"  (Oxford Nanopore)
                   pb: --olo "-x ava-pb" --mo "-x map-pb"     (PacBio)
```


# Test

```
cd test
./runTest.sh
```

## Outputs (-o)

Miniasm assembly unitig.gfa file and flye assembly assembly_graph.gfa can be viewed by [Bandage](http://rrwick.github.io/Bandage/)

example output using miniasm
```
|-/Assembly
   |-unitig.gfa 
   |-unitig.fasta
|-/Consensus
|-/ErrorCorrect
|- prefix.fa  <- The final contigs.fasta file
|- prefix.log
```


## Removing long_read_assembly

For removal, delete (`rm -rf`) `long_read_assembly` folder, which will remove any packages that were downloaded in that folder. 

## Citations

If you use this pipeline please cite following papers:

- **minimap2**: Li, H. (2018). Minimap2: pairwise alignment for nucleotide sequences. Bioinformatics, 34:3094-3100. 

- **miniasm**: Li. H. (2016). Minimap and miniasm: fast mapping and de novo assembly for noisy long sequences. Bioinformatics, Volume 32, Issue 14, 15 Pages 2103–2110. 

- **racon**: Robert Vaser et al. (2017). Fast and accurate de novo genome assembly from long uncorrected reads. Genome Research, 27: 737-746

- **wtdbg2**: Jue Ruan and Heng Li (2020). Fast and accurate long-read assembly with wtdbg2. Nat Methods 17, 155–158 (2020). https://doi.org/10.1038/s41592-019-0669-3

- **flye**: Kolmogorov M, Yuan J, Lin Y, Pevzner PA. (2019). Assembly of long, error-prone reads using repeat graphs. Nat Biotechnol. 2019;37(5):540-546. https://doi.org/10.1038/s41587-019-0072-8
