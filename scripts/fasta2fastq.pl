#!/usr/bin/env perl 
#convert fasta/qual into fastq
use strict;

my ($inFasta, $inQual, $outFastq);
my $ASCII=33; 

use Getopt::Long;
GetOptions('f=s', \$inFasta,
           'q=s', \$inQual,
	   'o=s', \$outFastq,
           'ASCII=i', \$ASCII
          );

usage() unless $inFasta  && $outFastq;

my %seqs;

$/ = ">";
my ($fh,$pid)= open_file($inFasta);

while (my $frecord = <$fh>) {
	next if $frecord =~ /^[ >]*$/;
	chomp $frecord;
	my ($fdef, @seqLines) = split /\n/, $frecord;
	my $seq = join '', @seqLines;
	$seqs{$fdef} = $seq;
}

close FASTA;

open (FASTQ, ">$outFastq");
if (!$inQual)
{
    my $qual_ascii=($ASCII==64)?"f":"9";
    foreach my $seq_id (keys %seqs)
    {
        my $seq = $seqs{$seq_id};
        my $seq_len = length($seq);
        
        print FASTQ "@","$seq_id\n";
        print FASTQ "$seq\n";
        print FASTQ "+\n";
        print FASTQ  $qual_ascii x $seq_len . "\n";
    } 
}
else
{
    open (QUAL, "<$inQual");

    while (my $qrecord = <QUAL>) {
	next if $qrecord =~ /^[ >]*$/;;
	chomp $qrecord;

	my ($qdef, @qualLines) = split /\n/, $qrecord;
	my $qualString = join ' ', @qualLines;
	my @quals = split /\s+/, $qualString;
	print FASTQ "@","$qdef\n";
	print FASTQ "$seqs{$qdef}\n";
	print FASTQ "+\n";
	foreach my $qual (@quals) {
		print FASTQ chr($qual + $ASCII);
	}
	print FASTQ "\n";
    }
    close QUAL;
}
close FASTQ;

#usage
sub usage {
print<<'end';
Usage:   fasta2fastq.pl [-ASCII <33|64> -q <quality file>] -f <fasta file> -o <output fastq>
                         ASCII default 33
                         Without quality file, it will assign fake quality values for the bases.
end
die;
}

sub open_file
{
    my ($file) = @_;
    my $fh;
    my $pid;
    if ( $file=~/\.gz$/i ) { $pid=open($fh, "gunzip -c $file |") or  err("gunzip -c $file: $!"); }
    else { $pid=open($fh,'<',$file) or err("$file: $!"); }
    return ($fh,$pid);
}

1;
